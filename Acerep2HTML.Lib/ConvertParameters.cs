﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acerep2HTML.Lib
{
    /// <summary>
    /// this class holds info about application config. It can be either read from file, generated in runtime or both
    /// </summary>
    public class ConvertParameters
    {

        
        /// <summary>
        /// path to file where Snap2Html template is stored
        /// </summary>
        public string HtmlTemplatePath { get; set; }
        /// <summary>
        /// Name of root folder. Default is "X:"
        /// </summary>
        public string RootFolderName { get; set; }
        public bool UseUnixTimeStamp { get; set; }
    }
}
