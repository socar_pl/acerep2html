﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acerep2HTML.Lib.DB
{

    //Table structure ID_FILE;ID_PARENT;IS_FOLDER;IS_DELETED;F_NAME;F_SIZE;F_ATTR;F_CREATE;F_UPDATE;F_ACCESS;F_FLAGS;F_CHILDREN_SIZE

    [Keyless]
    public class StructDriveTableModel
    {
        public int ID_FILE { get; set; }
        public int ID_PARENT { get; set; }
        public bool IS_FOLDER { get; set; }
        public bool IS_DELETED { get; set; }
        public string F_NAME { get; set; }
        public long F_SIZE { get; set; }
        public long F_ATTR { get; set; }
        public DateTime F_CREATE { get; set; }
        public DateTime F_UPDATE { get; set; }
        public DateTime F_ACCESS { get; set; }
        public long? F_FLAGS { get; set; }
        public long? F_CHILDREN_SIZE { get; set; }

    }



}
