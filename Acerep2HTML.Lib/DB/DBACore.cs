﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acerep2HTML.Lib.DB
{
    public static class DBACore
    {
        public static AcerapDBStructure BuildStructureFromDb(String sqliteConnectionString, string rootName)
        {
            AcerapDBStructure ads = new AcerapDBStructure(); //returned value 
            FileStructureElement root = new FileStructureElement();
            //root.Name = "X:"; //this should be moved to config

            root.Name = SanitizeRootName(rootName);
            root.ModificationTime = DateTime.Now;

            List<FileStructureElement> FSEList = new List<FileStructureElement>();

            using (AcerepDbContext db = new AcerepDbContext(sqliteConnectionString))
            {
                //Step1: Create flat structure of directories 
                
                var directories = db.struct_drive_table.Where(x => x.IS_FOLDER == true).ToList();
                foreach (var dir in directories)
                {
                    FileStructureElement fse = new FileStructureElement(dir);
                    FSEList.Add(fse);
                }

                //Step2: Update flat list of dirs with files 
                foreach(var dir in FSEList)
                {
                    var files = db.struct_drive_table.Where(x => x.IS_FOLDER == false && x.ID_PARENT == dir.DBID).ToList();
                    foreach (var file in files) //this could be done better with casting one type to another. maybe I'll fix it later
                    {
                        dir.Files.Add(new FileStructureElement(file));
                    }
                }

                //Step3: Build tree from directories
                foreach(var dir in FSEList)
                {
                    var res = FSEList.Where(x => x.acerepDatabaseRow.ID_PARENT == dir.DBID && x.isDir==true).ToList<FileStructureElement>();
                    if (res != null && res.Count() > 0)
                        dir.Dirs = res;
                }

                //Step3b: Update ParentElement property so we can traverse tree back and forth
                foreach (var dir in FSEList)
                {
                    if (dir.acerepDatabaseRow.ID_PARENT == -1)
                        dir.ParentElement = root;
                    else
                        dir.ParentElement = FSEList.Where(x => x.DBID == dir.acerepDatabaseRow.ID_PARENT).First();                    
                }


                //Step4: connect the root to the rest and update root files
                root.Dirs=(FSEList.Where(x => x.acerepDatabaseRow.ID_PARENT == -1).ToList());
                var rootFiles = db.struct_drive_table.Where(x => x.IS_FOLDER == false && x.ID_PARENT == -1).ToList();
                foreach(var rf in rootFiles)
                   root.Files.Add(new FileStructureElement(rf));

                //Step5: This library assume use of Snap2HTML as file tree render so we need to create additional index numbering so array generation can go smoothly [https://www.rlvision.com/snap2html]
                root.Snap2HtmlIndex = 0;
                long fseIndex = 1;
                foreach (var element in FSEList.OrderBy(x=>x.DBID))
                {
                    element.Snap2HtmlIndex = fseIndex;
                    fseIndex++;
                }

                //Generating stats
                ads.NumberOfFiles = db.struct_drive_table.Where(x => x.IS_FOLDER == false).Count();
                ads.NumberOfFiles_Deleted = db.struct_drive_table.Where(x => x.IS_FOLDER == false && x.IS_DELETED==true).Count();
                ads.NumberOfFiles_NotDeleted = ads.NumberOfFiles - ads.NumberOfFiles_Deleted;

                ads.NumberOfDirectories = db.struct_drive_table.Where(x => x.IS_FOLDER == true).Count();
                ads.NumberOfDirectories_Deleted = db.struct_drive_table.Where(x => x.IS_FOLDER == true && x.IS_DELETED == true).Count();
                ads.NumberOfDirectories_NotDeleted = ads.NumberOfDirectories - ads.NumberOfDirectories_Deleted;

                ads.TotalSize = (root.SizeOfFilesInDirectoryInKiloBytes + root.SizeOfDirectoriesInDirectoryInKiloBytes);
            }


            //Constructing response            
            ads.RootElement = root;
            ads.DirectoriesFlatList = FSEList;

            


            return ads;
        }

        private static string SanitizeRootName(string rootName)
        {
            if (!String.IsNullOrEmpty(rootName))
            {
                while(true)
                {
                    if (rootName.EndsWith("/"))
                    {
                        rootName = rootName.TrimEnd('/');
                    }
                    else
                        return rootName;
                }
            }
            else
                throw new Exception("Root name is empty");
        }
    }

    public class AcerapDBStructure
    {
        public FileStructureElement RootElement { get; set; }
        public List<FileStructureElement> DirectoriesFlatList { get; set; }

        public int NumberOfFiles { get; set; }
        public int NumberOfFiles_NotDeleted { get; set; }
        public int NumberOfFiles_Deleted { get; set; }
        public int NumberOfDirectories { get; internal set; }
        public int NumberOfDirectories_Deleted { get; internal set; }
        public int NumberOfDirectories_NotDeleted { get; internal set; }
        public long TotalSize { get; internal set; }
    }
}
