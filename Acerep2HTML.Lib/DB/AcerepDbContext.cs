﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acerep2HTML.Lib.DB
{
    public class AcerepDbContext : DbContext
    {
        private string _sqliteConnectionString = "";
        public AcerepDbContext(string SqliteConnectionString)
        {
            _sqliteConnectionString = SqliteConnectionString;
        }
       public DbSet<StructDriveTableModel> struct_drive_table { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlite(_sqliteConnectionString);
    }
}

