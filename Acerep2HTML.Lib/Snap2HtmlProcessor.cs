﻿using Acerep2HTML.Lib.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acerep2HTML.Lib
{
    public class Snap2HtmlProcessor
    {
        private ConvertParameters _config = null;
        public Snap2HtmlProcessor(ConvertParameters config)
        {
            if (config == null)
                throw new Exception("Config cannot be null!");

            _config = config;
        }


        public string DirStructureToS2HArrayAsJSString(AcerapDBStructure data, string NewLineSeparator = null)
        {
            if (NewLineSeparator == null)
                NewLineSeparator = Environment.NewLine;

            var jsstructure = this.DirStructureToS2HArray(data);
            string s = String.Join(NewLineSeparator, jsstructure);
            return s;
        }

        /// <summary>
        /// Converts data read from sqlite database (*.acerep file) to list of strings required for [DIR DATA]
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public List<string> DirStructureToS2HArray(AcerapDBStructure data)
        {
            List<string> jsArrayLines = new List<string>(); //lines of javascript code which will be them collated and placed inside HTML template
            var jsArrayIndexOrderedDirectoryList = data.DirectoriesFlatList.OrderBy(x=>x.Snap2HtmlIndex).ToList();

            //Add root element first            
            jsArrayLines.Add(GetJsStringFromFSE(data.RootElement,true));

            //Add rest of the directories
            foreach (var dir in jsArrayIndexOrderedDirectoryList)
            {
                jsArrayLines.Add(GetJsStringFromFSE(dir));
            }

            return jsArrayLines;                     
        }

        public string GetJsStringFromFSE(FileStructureElement fse, bool isRootElement=false)
        {
            String linePrefix = "D.p([";
            String linePostfix = "])";

            //line template:
            //[SEGMENT1][  SEGMENT 2  ][  SEGMENT 3  ][     SEGMENT 4     ]
            //[DIR_PATH][LIST_OF_FILES][SIZE_IN_BYTES][DIRECTORY_REFERENCE]
                       
            //INTRO
            StringBuilder rootRow = new StringBuilder();
            rootRow.Append(linePrefix);

            //SEGMENT 1
            rootRow.Append(fse.S2H_GetDirString(isRootElement, _config.UseUnixTimeStamp));
            rootRow.Append(",");

            //SEGMENT 2
            string files = fse.S2H_GetFilesString(_config.UseUnixTimeStamp);
            if (!String.IsNullOrEmpty(files))
            {
                rootRow.Append(files);
                rootRow.Append(",");
            }

            //SEGMENT 3
            rootRow.Append(fse.SizeOfFilesInDirectoryInKiloBytes); //temp just to test, this should have a global size or something
            rootRow.Append(",");

            //SEGMENT 4
            rootRow.Append("\"");
            rootRow.Append(fse.S2H_GetChildDirectoriesArrayIndexReferenceString());
            rootRow.Append("\"");

            //OUTRO
            rootRow.Append(linePostfix);

            return rootRow.ToString();
        }

        /// <summary>
        /// Loads template from disk, replaces all placeholders in it with provided data and returns ready document as string
        /// </summary>
        /// <param name="pValues">placeholder names and values that shall be put in their place</param>
        /// <returns>Template text with placeholders replaced by provided values</returns>
        /// <exception cref="Exception"></exception>
        public StringBuilder InflateTemplate(PlaceholderReplacer pValues)
        {
            if (_config != null && File.Exists(_config.HtmlTemplatePath))
            {
                var html = new StringBuilder(File.ReadAllText(_config.HtmlTemplatePath));
                var s = pValues.ExecuteReplacement(html);
                return s;
            }
            else
                throw new Exception("Config is incorrect"); //that's vague -- to fix
        }

        #region segments explanation
        /* Example javascript listing from sample file
D.p(["E:/*0*315525600",0,"1"])
D.p(["E:/X*0*1661509660","le_file.txt*0*1661546260",0,"2*5"])
D.p(["E:/X/dir1*0*1661511370","drugiplik.txt*16*1661511374","file2.txt*0*1661509688",16,"3"])
D.p(["E:/X/dir1/dir3*0*1661509672",0,"4"])
D.p(["E:/X/dir1/dir3/dir4*0*1661509678","file1.txt*0*1661509676",0,""])
D.p(["E:/X/dir2*0*1661509696","file3.vsdx*0*1661509694",0,""])

           	Data format description by Snap2HTML Author:
				Each index in "dirs" array is an array representing a directory:
					First item in array: "directory path*always 0*directory modified date"
						Note that forward slashes are used instead of (Windows style) backslashes
					Then, for each each file in the directory: "filename*size of file*file modified date"
					Second to last item in array tells the total size of directory content
					Last item in array refrences IDs to all subdirectories of this dir (if any).
						ID is the item index in dirs array.
				Note: Modified date is in UNIX format


           SOCAR comment:
        1) each item consist of 
           A = "path*size_in_bytes*modification_date_unix_timstamp", 
           B = list of files 
           C = size_of_dir_0, 
           D = list of directories. This list is based on javascript array reference to each directory position and separated by asterisk (*)                  

         */
        #endregion

    }
}
