﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acerep2HTML.Lib
{

    static class DateTimeExtension
    {
        /// <summary>
        /// Gets unix timestamp from a datetime
        /// </summary>
        /// <param name="dt">DateTime to get the unix timestamp from</param>
        /// <returns>Unix timestamp</returns>
        public static long GetUnixTimestamp(this DateTime dt)
        {
            DateTimeOffset dto = new DateTimeOffset(dt);
            return dto.ToUnixTimeSeconds();

        }
    }

}
