﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acerep2HTML.Lib
{
    public class PlaceholderReplacer
    {
        private List<S2HPlaceholder> _placeholderValues = null;
        public PlaceholderReplacer(List<S2HPlaceholder> placeholderValues)
        {
            _placeholderValues = placeholderValues;
        }

        public StringBuilder ExecuteReplacement(StringBuilder inputText)
        {
            foreach (var pv in _placeholderValues)
                inputText = pv.ReplacePlaceholderWithValue(inputText);

            return inputText;
        }

        /// <summary>
        /// This generate default placeholder set. Can be used as test data
        /// </summary>
        /// <returns></returns>
        public static List<S2HPlaceholder> GetDefaultPlaceholderSet()
        {
            List<S2HPlaceholder> placeholderSet = new List<S2HPlaceholder>();
            placeholderSet.Add(new S2HPlaceholder("[APP NAME]", "Acerep2HTML"));
            placeholderSet.Add(new S2HPlaceholder("[APP VER]", "0.1"));
            placeholderSet.Add(new S2HPlaceholder("[GEN DATE]", DateTime.Now.ToString("yyyy-MM-dd")));
            placeholderSet.Add(new S2HPlaceholder("[GEN TIME]", DateTime.Now.ToString("HH:mm:ss")));
            placeholderSet.Add(new S2HPlaceholder("[APP LINK]", "https://gitlab.com/socar_pl/acerep2html"));
            placeholderSet.Add(new S2HPlaceholder("[TITLE]", "File report"));
            placeholderSet.Add(new S2HPlaceholder(S2HPlaceholder.Placeholders.DIR_DATA, "D.p([\"X:/*0*)\",0,\"\"])"));
            placeholderSet.Add(new S2HPlaceholder("[NUM FILES]", "0"));
            placeholderSet.Add(new S2HPlaceholder("[LINK FILES]", "false"));
            placeholderSet.Add(new S2HPlaceholder("[LINK PROTOCOL]", ""));
            placeholderSet.Add(new S2HPlaceholder("[LINK ROOT]", ""));
            placeholderSet.Add(new S2HPlaceholder("[SOURCE ROOT]", "X:/"));
            placeholderSet.Add(new S2HPlaceholder("[NUM DIRS]", "0"));
            placeholderSet.Add(new S2HPlaceholder("[TOT SIZE]", "0"));

            return placeholderSet;
        }





    }




    public class S2HPlaceholder
    {

        public S2HPlaceholder(Placeholders pName, string targetValue) : this(S2HPlaceholder.PlaceholderToPlaceholderText(pName), targetValue)
        {

        }
        public S2HPlaceholder(string placeholder, string targetValue)
        {
            this.Placeholder = placeholder;
            this.Value = targetValue;
            this._placeholderEnum = (Placeholders)Array.IndexOf(_placeholderTextValues, placeholder);
        }

        public static string PlaceholderToPlaceholderText(Placeholders p)
        {
            return _placeholderTextValues[(int)p];
        }

        /// <summary>
        /// This is the placeholder string that will be replaced with the value. 
        /// Example: [DIR DATA], [GEN TIME], etc
        /// </summary>
        public string Placeholder { get; set; }
        /// <summary>
        /// Property contains value that will be placed in place of the placeholder ;)
        /// </summary>
        public string Value { get; set; }

        public Placeholders PlaceholderType
        {
            get
            {
                return _placeholderEnum;
            }
        }


        private Placeholders _placeholderEnum = Placeholders.NONE;

        public StringBuilder ReplacePlaceholderWithValue(StringBuilder source)
        {
            return source.Replace(Placeholder, Value);
        }

        public override string ToString()
        {
            return String.Format($"{this.Placeholder} => {this.Value}");
        }

        public enum Placeholders : int { APP_NAME, APP_VER, GEN_DATE, GEN_TIME, APP_LINK, TITLE, DIR_DATA, NUM_FILES, LINK_FILES, LINK_PROTOCOL, LINK_ROOT, SOURCE_ROOT, NUM_DIRS, TOT_SIZE, NONE }; //keep [NONE] last combined with "" 
        private static readonly string[] _placeholderTextValues = { "[APP NAME]", "[APP VER]", "[GEN DATE]", "[GEN TIME]", "[APP LINK]", "[TITLE]", "[DIR DATA]", "[NUM FILES]", "[LINK FILES]", "[LINK PROTOCOL]", "[LINK ROOT]", "[SOURCE ROOT]", "[NUM DIRS]", "[TOT SIZE]", "" };

    }
}
