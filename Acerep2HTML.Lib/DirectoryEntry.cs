﻿using Acerep2HTML.Lib.DB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Acerep2HTML.Lib
{
    public class FileStructureElement
    {
        public FileStructureElement()
        {

        }

        private StructDriveTableModel _sdtmElement = null;
        public FileStructureElement ParentElement { get; set; }
        public FileStructureElement(StructDriveTableModel sdtmElement)
        {
            _sdtmElement = sdtmElement;
            this.Name = _sdtmElement.F_NAME;
            this.ModificationTime = _sdtmElement.F_UPDATE;
            this.DBID = _sdtmElement.ID_FILE;
            this.isDir = _sdtmElement.IS_FOLDER;
        }

        public string Path
        {
            get
            {
                // ...parent-path... + name;
                string prefix = "";
                if (this.ParentElement != null)
                    prefix = this.ParentElement.Path+"/";

                return prefix + this.Name;
            }
        }

        public long Snap2HtmlIndex { get;set; }

        public StructDriveTableModel acerepDatabaseRow
        {
            get
            {
                return _sdtmElement;
            }

            set
            {
                _sdtmElement = value;
            }
        }

        public long SizeOfFilesInDirectoryInKiloBytes
        {
            get
            {
                //this is a "dynamic" calculation, which for my taste is slow. It should be done after whole dataset is loaded and triggered as one time calculation thru whole tree. 
                //For now I will keep it as overall process time should be acceptable for current processors. 

                long result = 0;
                foreach (var file in _childFiles)
                    result += file.acerepDatabaseRow.F_SIZE;


                return result;

            }
        }

        public long SizeOfDirectoriesInDirectoryInKiloBytes
        {
            get
            {
                //this is a "dynamic" calculation, which for my taste is slow. It should be done after whole dataset is loaded and triggered as one time calculation thru whole tree. 
                //For now I will keep it as overall process time should be acceptable for current processors. 

                long result = 0;
                foreach (var dir in _childDirs)
                {
                    result += dir.SizeOfFilesInDirectoryInKiloBytes;
                    result += dir.SizeOfDirectoriesInDirectoryInKiloBytes;
                }


                return result;
            }
        }

        


        public override string ToString()
        {
            return this.Name;
        }

        public int DBID { get; set; } //this value is taken from the database ID_FILE field
        public bool isDir { get; set; }
        private List<FileStructureElement> _childFiles = new List<FileStructureElement>();
        private List<FileStructureElement> _childDirs = new List<FileStructureElement>();
        public string Name { get; set; }
        public DateTime ModificationTime { get; set; }

        public List<FileStructureElement> Files { get { return _childFiles; } set { _childFiles = value; } }
        public List<FileStructureElement> Dirs { get { return _childDirs; } set { _childDirs = value; } }

        public void AddElement(StructDriveTableModel entry)
        {
            if (entry != null)
            {
                if (entry.IS_FOLDER)
                {
                    _childDirs.Add(new FileStructureElement(entry));
                }
                else
                {
                    _childFiles.Add(new FileStructureElement(entry));
                }
            }
        }

        /// <summary>
        /// Returns file list for directory entry         
        /// </summary>
        /// <returns></returns>
        public string S2H_GetFilesString(bool useUnixTimeStamp=true)
        {
            if(this.Files==null || this.Files.Count()==0)
                return "";

            //"file1.txt*0*1661509676"
            List<string> response = new List<string>();

            foreach(var file in this.Files)
            {
                string timeToInsert = useUnixTimeStamp ? file.ModificationTime.GetUnixTimestamp().ToString() : file.ModificationTime.ToString("yyyy-MM-dd HH:mm:ss");
                string x = $"\"{file.Name}*{file.acerepDatabaseRow.F_SIZE}*{timeToInsert}\"";
                response.Add(x);
            }

            return String.Join(",",response);            
        }

        public string S2H_GetDirString(bool AppendSlashAfterResolvePath=false, bool useUnixTimeStamp=true)
        {
            StringBuilder resp = new StringBuilder();
            resp.Append("\"");
            resp.Append(this.Path);
            if(AppendSlashAfterResolvePath)
                resp.Append("/");
            resp.Append($"*0*");
            resp.Append(useUnixTimeStamp?this.ModificationTime.GetUnixTimestamp().ToString():this.ModificationTime.ToString("yyyy-MM-dd HH:mm:ss")); 
            resp.Append("\"");

            return resp.ToString();
        }

        /// <summary>
        /// Returns array index numbers separated with * for child directories of current directory
        /// </summary>
        /// <returns></returns>
        public string S2H_GetChildDirectoriesArrayIndexReferenceString()
        {
            
            String resultStr = "";
            if (this._childDirs.Count() > 0)
            {
                var childDirsSnap2HtmlIndex = _childDirs.Select(x => x.Snap2HtmlIndex).ToList();
                childDirsSnap2HtmlIndex.Sort();
                resultStr = String.Join("*", childDirsSnap2HtmlIndex);               
            }

            return resultStr;
        }
    }

  
}
