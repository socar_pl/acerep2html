using Acerep2HTML.Lib;
using Acerep2HTML.Lib.DB;
using System.Diagnostics;

namespace Acerep2HTML.Tests
{
    [TestClass]
    public class DatabaseConnectionTests
    {
/*
        public string SqliteTestDBString = "Data Source=\"C:\\c\\acerep\\a.sqlite\"";
        [TestMethod]
        public void T001_ConnectToDB()
        {
            using (AcerepDbContext context1 = new AcerepDbContext(SqliteTestDBString))
            {
                
                var elementcount = context1.struct_drive_table.Count();
                Trace.WriteLine($"{elementcount} elements found in struct_drive_table");
            }
        }


        [TestMethod]
        public void T002_Debug_BuildTreeFromDB()
        {
            var result = DBACore.BuildStructureFromDb(SqliteTestDBString);
            
        }

        [TestMethod]
        public void T003_Debug_S2HPRocessor()
        {
            var result = DBACore.BuildStructureFromDb(SqliteTestDBString);
            ConvertParameters ac = new ConvertParameters();
            ac.HtmlTemplatePath = @"C:\c\acerep\orig-template.html";
            Snap2HtmlProcessor processor = new Snap2HtmlProcessor(ac);
            var jsstructure = processor.DirStructureToS2HArray(result);

            string s = String.Join(Environment.NewLine, jsstructure);
            File.WriteAllText("C:\\c\\alltext1.txt", s);

        }

        [TestMethod]
        public void T004_GeneateTemplateWithDefaultValues()
        {
            ConvertParameters ac = new ConvertParameters();
            ac.HtmlTemplatePath = @"C:\c\acerep\orig-template.html";

            string tmpOutput = $@"C:\c\acerep\test-result-{DateTime.Now.ToString("yyyyMMddHHmmss")}.html";

            Snap2HtmlProcessor s2hProcessor = new Snap2HtmlProcessor(ac);
            var updated = s2hProcessor.InflateTemplate(new PlaceholderReplacer(PlaceholderReplacer.GetDefaultPlaceholderSet()));

            File.WriteAllText(tmpOutput, updated.ToString());
            Trace.WriteLine(tmpOutput);
        }


        [TestMethod]
        public void T005_GeneateTemplateWithTestFileInfoValues()
        {
            ConvertParameters ac = new ConvertParameters();
            ac.HtmlTemplatePath = @"C:\c\acerep\orig-template.html";

            string tmpOutput = $@"C:\c\acerep\test-result-{DateTime.Now.ToString("yyyyMMddHHmmss")}.html";

            Snap2HtmlProcessor s2hProcessor = new Snap2HtmlProcessor(ac);
            var temp = PlaceholderReplacer.GetDefaultPlaceholderSet();

            //Get data from database
            var result = DBACore.BuildStructureFromDb(SqliteTestDBString);
            Snap2HtmlProcessor processor = new Snap2HtmlProcessor(ac);
            var jsstructure = processor.DirStructureToS2HArray(result);
            string s = String.Join(Environment.NewLine, jsstructure);

            //Put data in placeholderReplacer
            temp.Where(x => x.PlaceholderType == S2HPlaceholder.Placeholders.DIR_DATA).First().Value = s;
            temp.Where(x => x.PlaceholderType == S2HPlaceholder.Placeholders.NUM_DIRS).First().Value = result.NumberOfDirectories.ToString();
            temp.Where(x => x.PlaceholderType == S2HPlaceholder.Placeholders.NUM_FILES).First().Value = result.NumberOfFiles.ToString();


            //Get updated template
            var pl = new PlaceholderReplacer(temp);
            var updated = s2hProcessor.InflateTemplate(pl);

            File.WriteAllText(tmpOutput, updated.ToString());
            Trace.WriteLine(tmpOutput);
        }

        */
    }
}