﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Acerep2HTML.Win32
{
    public partial class AppSettings : Form
    {
        private ApplicationSettings globalAppSettings;

        public AppSettings(ApplicationSettings globalAppSettings)
        {
            this.globalAppSettings = globalAppSettings;
            InitializeComponent();
            propertyGrid1.SelectedObject = this.globalAppSettings;

        }

        private void AppSettings_Load(object sender, EventArgs e)
        {
            
        }

        private void _button_Save_Click(object sender, EventArgs e)
        {
            var v = propertyGrid1.SelectedObject as ApplicationSettings;
            ApplicationSettings.Save(Program.AppSettingsPath, v);

            this.DialogResult = DialogResult.OK;
            this.Close();            
        }

        

        private void _button_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
