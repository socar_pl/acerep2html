﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Acerep2HTML.Win32
{
    public class ApplicationSettings
    {
        public static ApplicationSettings DefaultConfig
        {
            get
            {
                ApplicationSettings settings = new ApplicationSettings();
                settings.OpenHTMLAfterConversion = true;
                settings.RootFolderName = "X:/";
                settings.HTMLTemplatePath = @".\snap2html-template.html";
                settings.UseUnixTimeStamp = true;
                settings.UseStaticFolderName = false;

                return settings;
            }


        }

        private string _htmlTemplatePath = "";

        public bool OpenHTMLAfterConversion { get; set; }
        public string HTMLTemplatePath {


            get {
                if (_htmlTemplatePath.StartsWith("."))
                {
                    string tmp = _htmlTemplatePath.Substring(2);
                    string x= Path.Combine(Program.AppLocation, tmp);
                    return x;
                }
                else
                    return _htmlTemplatePath;
            
            }
            set
            {
                _htmlTemplatePath = value;
            }
        }

        public bool UseUnixTimeStamp { get;  set; }
        public bool UseStaticFolderName { get; set; }
        public string RootFolderName { get; set; }

        public static ApplicationSettings Load(string path)
        {
            string json = File.ReadAllText(path);
            var settings = JsonSerializer.Deserialize<ApplicationSettings>(json);
            return (ApplicationSettings)settings;
        }

        public static void Save(string path, ApplicationSettings obj)
        {
            var json = JsonSerializer.Serialize(obj, typeof(ApplicationSettings), new JsonSerializerOptions() { WriteIndented = true }); ;
            File.WriteAllText(path, json);
        }
    }
}
