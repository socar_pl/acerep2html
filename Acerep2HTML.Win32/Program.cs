namespace Acerep2HTML.Win32
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (!File.Exists(Program.AppSettingsPath))
                ApplicationSettings.Save(Program.AppSettingsPath, ApplicationSettings.DefaultConfig);

            GlobalAppSettings = ApplicationSettings.Load(Program.AppSettingsPath);
            
            ApplicationConfiguration.Initialize();
            Application.Run(new MainForm());
        }

        public static string AppLocation
        {
            get
            {
                return AppContext.BaseDirectory;
            }
        }

        private static string myAppConfig = "appconfig.json";

        public static string AppSettingsPath
        {
            get
            {
                return System.IO.Path.Combine(Program.AppLocation, myAppConfig);
            }
        }
        private static ApplicationSettings _globalAppSettings = null;
        public static ApplicationSettings GlobalAppSettings
        {
            get
            {
                return _globalAppSettings;
            }

            set
            {
                _globalAppSettings = value;
            }
        }

        public static string Version
        {
            get
            {
                return "0.35";
            }
        }


    }
}