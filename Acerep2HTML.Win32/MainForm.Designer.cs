﻿namespace Acerep2HTML.Win32
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._button_OpenInputFile = new System.Windows.Forms.Button();
            this._text_InputFilePath = new System.Windows.Forms.TextBox();
            this._text_OutputFilePath = new System.Windows.Forms.TextBox();
            this._button_OutputFilePath = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._ts_Status = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this._ts_Version = new System.Windows.Forms.ToolStripStatusLabel();
            this._check_OpenOnComplete = new System.Windows.Forms.CheckBox();
            this._button_Convert = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this._ts_Button_About = new System.Windows.Forms.ToolStripButton();
            this._ts_Button_Settings = new System.Windows.Forms.ToolStripButton();
            this._text_RootName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.statusStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _button_OpenInputFile
            // 
            this._button_OpenInputFile.Location = new System.Drawing.Point(613, 55);
            this._button_OpenInputFile.Name = "_button_OpenInputFile";
            this._button_OpenInputFile.Size = new System.Drawing.Size(41, 29);
            this._button_OpenInputFile.TabIndex = 0;
            this._button_OpenInputFile.Text = "...";
            this._button_OpenInputFile.UseVisualStyleBackColor = true;
            this._button_OpenInputFile.Click += new System.EventHandler(this._button_OpenInputFile_Click);
            // 
            // _text_InputFilePath
            // 
            this._text_InputFilePath.Location = new System.Drawing.Point(123, 57);
            this._text_InputFilePath.Name = "_text_InputFilePath";
            this._text_InputFilePath.Size = new System.Drawing.Size(484, 27);
            this._text_InputFilePath.TabIndex = 1;
            // 
            // _text_OutputFilePath
            // 
            this._text_OutputFilePath.Location = new System.Drawing.Point(123, 93);
            this._text_OutputFilePath.Name = "_text_OutputFilePath";
            this._text_OutputFilePath.Size = new System.Drawing.Size(484, 27);
            this._text_OutputFilePath.TabIndex = 1;
            // 
            // _button_OutputFilePath
            // 
            this._button_OutputFilePath.Location = new System.Drawing.Point(613, 92);
            this._button_OutputFilePath.Name = "_button_OutputFilePath";
            this._button_OutputFilePath.Size = new System.Drawing.Size(41, 29);
            this._button_OutputFilePath.TabIndex = 0;
            this._button_OutputFilePath.Text = "...";
            this._button_OutputFilePath.UseVisualStyleBackColor = true;
            this._button_OutputFilePath.Click += new System.EventHandler(this._button_OutputFilePath_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Input file: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 96);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Output file: ";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ts_Status,
            this.toolStripStatusLabel1,
            this._ts_Version});
            this.statusStrip1.Location = new System.Drawing.Point(0, 295);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(666, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // _ts_Status
            // 
            this._ts_Status.Name = "_ts_Status";
            this._ts_Status.Size = new System.Drawing.Size(0, 16);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(651, 16);
            this.toolStripStatusLabel1.Spring = true;
            // 
            // _ts_Version
            // 
            this._ts_Version.Name = "_ts_Version";
            this._ts_Version.Size = new System.Drawing.Size(0, 16);
            this._ts_Version.ToolTipText = "Version";
            // 
            // _check_OpenOnComplete
            // 
            this._check_OpenOnComplete.AutoSize = true;
            this._check_OpenOnComplete.Checked = true;
            this._check_OpenOnComplete.CheckState = System.Windows.Forms.CheckState.Checked;
            this._check_OpenOnComplete.Location = new System.Drawing.Point(226, 184);
            this._check_OpenOnComplete.Name = "_check_OpenOnComplete";
            this._check_OpenOnComplete.Size = new System.Drawing.Size(230, 24);
            this._check_OpenOnComplete.TabIndex = 4;
            this._check_OpenOnComplete.Text = "Open on conversion complete";
            this._check_OpenOnComplete.UseVisualStyleBackColor = true;
            // 
            // _button_Convert
            // 
            this._button_Convert.Location = new System.Drawing.Point(248, 226);
            this._button_Convert.Name = "_button_Convert";
            this._button_Convert.Size = new System.Drawing.Size(179, 42);
            this._button_Convert.TabIndex = 6;
            this._button_Convert.Text = "Convert";
            this._button_Convert.UseVisualStyleBackColor = true;
            this._button_Convert.Click += new System.EventHandler(this._button_Convert_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "*.acerep";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Acerep file (*.acerep)|*.acerep|All files (*.*)|*.*";
            this.openFileDialog1.Title = "Open soruce ACEREP file";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this._ts_Button_About,
            this._ts_Button_Settings});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(666, 27);
            this.toolStrip1.TabIndex = 7;
            this.toolStrip1.Text = "toolStrip1";
            this.toolStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.toolStrip1_ItemClicked);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::Acerep2HTML.Win32.Properties.Resources.Internet_Explorer_icon;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(29, 24);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // _ts_Button_About
            // 
            this._ts_Button_About.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._ts_Button_About.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._ts_Button_About.Image = global::Acerep2HTML.Win32.Properties.Resources.Help_icon;
            this._ts_Button_About.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ts_Button_About.Name = "_ts_Button_About";
            this._ts_Button_About.Size = new System.Drawing.Size(29, 24);
            this._ts_Button_About.Text = "toolStripButton1";
            this._ts_Button_About.Click += new System.EventHandler(this._ts_Button_About_Click);
            // 
            // _ts_Button_Settings
            // 
            this._ts_Button_Settings.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this._ts_Button_Settings.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._ts_Button_Settings.Image = global::Acerep2HTML.Win32.Properties.Resources.Settings_icon2;
            this._ts_Button_Settings.ImageTransparentColor = System.Drawing.Color.Magenta;
            this._ts_Button_Settings.Name = "_ts_Button_Settings";
            this._ts_Button_Settings.Size = new System.Drawing.Size(29, 24);
            this._ts_Button_Settings.Text = "toolStripButton1";
            this._ts_Button_Settings.Click += new System.EventHandler(this._ts_Button_Settings_Click);
            // 
            // _text_RootName
            // 
            this._text_RootName.Location = new System.Drawing.Point(123, 126);
            this._text_RootName.Name = "_text_RootName";
            this._text_RootName.Size = new System.Drawing.Size(484, 27);
            this._text_RootName.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(30, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 20);
            this.label3.TabIndex = 9;
            this.label3.Text = "Root name:";
            // 
            // MainForm
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(666, 317);
            this.Controls.Add(this.label3);
            this.Controls.Add(this._text_RootName);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this._button_Convert);
            this.Controls.Add(this._check_OpenOnComplete);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this._text_OutputFilePath);
            this.Controls.Add(this._button_OutputFilePath);
            this.Controls.Add(this._text_InputFilePath);
            this.Controls.Add(this._button_OpenInputFile);
            this.MaximumSize = new System.Drawing.Size(684, 364);
            this.MinimumSize = new System.Drawing.Size(684, 364);
            this.Name = "MainForm";
            this.Text = "Acerep2HTML";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainForm_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.MainForm_DragEnter);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button _button_OpenInputFile;
        private TextBox _text_InputFilePath;
        private TextBox _text_OutputFilePath;
        private Button _button_OutputFilePath;
        private Label label1;
        private Label label2;
        private StatusStrip statusStrip1;
        private ToolStripStatusLabel _ts_Status;
        private ToolStripStatusLabel _ts_Version;
        private CheckBox checkBox1;
        private Button _button_Convert;
        private SaveFileDialog saveFileDialog1;
        private OpenFileDialog openFileDialog1;
        private ToolStrip toolStrip1;
        private ToolStripButton _ts_Button_About;
        private ToolStripButton _ts_Button_Settings;
        private CheckBox _check_OpenOnComplete;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private TextBox _text_RootName;
        private Label label3;
        private ToolStripButton toolStripButton1;
    }
}