﻿namespace Acerep2HTML.Win32
{
    partial class AppSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this._button_Save = new System.Windows.Forms.Button();
            this._button_Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.propertyGrid1.Location = new System.Drawing.Point(12, 12);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new System.Drawing.Size(496, 741);
            this.propertyGrid1.TabIndex = 0;
            // 
            // _button_Save
            // 
            this._button_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._button_Save.Location = new System.Drawing.Point(314, 759);
            this._button_Save.Name = "_button_Save";
            this._button_Save.Size = new System.Drawing.Size(94, 29);
            this._button_Save.TabIndex = 1;
            this._button_Save.Text = "Save";
            this._button_Save.UseVisualStyleBackColor = true;
            this._button_Save.Click += new System.EventHandler(this._button_Save_Click);
            // 
            // _button_Cancel
            // 
            this._button_Cancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._button_Cancel.Location = new System.Drawing.Point(414, 759);
            this._button_Cancel.Name = "_button_Cancel";
            this._button_Cancel.Size = new System.Drawing.Size(94, 29);
            this._button_Cancel.TabIndex = 2;
            this._button_Cancel.Text = "Cancel";
            this._button_Cancel.UseVisualStyleBackColor = true;
            this._button_Cancel.Click += new System.EventHandler(this._button_Cancel_Click);
            // 
            // AppSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(520, 800);
            this.Controls.Add(this._button_Cancel);
            this.Controls.Add(this._button_Save);
            this.Controls.Add(this.propertyGrid1);
            this.Name = "AppSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Application settings";
            this.Load += new System.EventHandler(this.AppSettings_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private PropertyGrid propertyGrid1;
        private Button _button_Save;
        private Button _button_Cancel;
    }
}