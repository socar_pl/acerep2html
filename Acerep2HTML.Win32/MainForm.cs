using Acerep2HTML.Lib.DB;
using Acerep2HTML.Lib;
using System.Diagnostics;

namespace Acerep2HTML.Win32
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

            _check_OpenOnComplete.Checked = Program.GlobalAppSettings.OpenHTMLAfterConversion;
            RefreshRootNameTextInput();
            this._ts_Version.Text = $"ver. {Program.Version}";
        }

        private void _button_OpenInputFile_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if (!File.Exists(openFileDialog1.FileName))
                {
                    MessageBox.Show("File does not exist", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                _text_InputFilePath.Text = openFileDialog1.FileName;
                RefreshUIAfterInputFileSet();
            }
        }

        public void RefreshUIAfterInputFileSet()
        {
            _text_OutputFilePath.Text = GetOutputName(_text_InputFilePath.Text);
            RefreshRootNameTextInput();
        }

        private string GetOutputName(string inputName)
        {
            FileInfo fi = new FileInfo(inputName);

            string x = "";
            int fileIndex = 0;
            while (true)
            {
                x = fi.FullName.Substring(0, fi.FullName.LastIndexOf('.'));
                if (fileIndex > 0)
                    x += $" ({fileIndex})";



                if (!File.Exists(x + ".html"))
                    break;
                else
                    fileIndex++;

            }

            x += ".html";

            return x;
        }

        private void _ts_Button_Settings_Click(object sender, EventArgs e)
        {
            AppSettings asf = new AppSettings(Program.GlobalAppSettings);

            if (asf.ShowDialog() == DialogResult.OK)
            {
                _check_OpenOnComplete.Checked = Program.GlobalAppSettings.OpenHTMLAfterConversion;

                RefreshRootNameTextInput();
            }
        }

        private void _button_Convert_Click(object sender, EventArgs e)
        {
            if (FilePathsValid())
            {
                try
                {
                    string input = _text_InputFilePath.Text;
                    string output = _text_OutputFilePath.Text;
                    string rootFolderName = _text_RootName.Text; 

                    ExecuteACEREP2HTML(input, output, rootFolderName);

                    if (_check_OpenOnComplete.Checked)
                    {
                        ProcessStartInfo psi = new ProcessStartInfo();
                        psi.FileName = "explorer.exe";
                        psi.Arguments = $"\"{output}\"";
                        Process.Start(psi);
                    }

                    _ts_Status.Text = $"[{DateTime.Now.ToString("HH:mm:ss")}] Done!";
                    System.Media.SystemSounds.Asterisk.Play();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Exception occured", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
        }

        private static void ExecuteACEREP2HTML(string input, string output, string RootFolderName=null)
        {
            string SqliteTestDBString = $"Data Source=\"{input}\"";

            ConvertParameters ac = new ConvertParameters(); //create interface for the constructor at .Lib
            ac.HtmlTemplatePath = Program.GlobalAppSettings.HTMLTemplatePath;
            ac.RootFolderName = Program.GlobalAppSettings.RootFolderName;
            ac.UseUnixTimeStamp = Program.GlobalAppSettings.UseUnixTimeStamp;
            ac.RootFolderName = RootFolderName==null?Program.GlobalAppSettings.RootFolderName:RootFolderName;

            Snap2HtmlProcessor s2hProcessor = new Snap2HtmlProcessor(ac);
            var temp = PlaceholderReplacer.GetDefaultPlaceholderSet();


            //Get data from database
            var result = DBACore.BuildStructureFromDb(SqliteTestDBString, ac.RootFolderName);

            //Initialize Snap2HTML Processor with app settings
            Snap2HtmlProcessor processor = new Snap2HtmlProcessor(ac);

            //Get Javascript Arrays describing directories 
            var jsString = processor.DirStructureToS2HArrayAsJSString(result);

            //Put data in placeholderReplacer
            temp.Where(x => x.PlaceholderType == S2HPlaceholder.Placeholders.DIR_DATA).First().Value = jsString;
            temp.Where(x => x.PlaceholderType == S2HPlaceholder.Placeholders.NUM_DIRS).First().Value = result.NumberOfDirectories.ToString();
            temp.Where(x => x.PlaceholderType == S2HPlaceholder.Placeholders.NUM_FILES).First().Value = result.NumberOfFiles.ToString();
            temp.Where(x => x.PlaceholderType == S2HPlaceholder.Placeholders.TOT_SIZE).First().Value = result.TotalSize.ToString();
            temp.Where(x => x.PlaceholderType == S2HPlaceholder.Placeholders.SOURCE_ROOT).First().Value = ac.RootFolderName;


            //Get updated template
            var pl = new PlaceholderReplacer(temp);
            var updated = s2hProcessor.InflateTemplate(pl);

            File.WriteAllText(output, updated.ToString());
        }

        private bool FilePathsValid()
        {
            if(String.IsNullOrEmpty(_text_RootName.Text))
            {
                MessageBox.Show("Root name cannot be empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!String.IsNullOrEmpty(_text_RootName.Text) && _text_RootName.Text.EndsWith(":/") == false)
            {
                MessageBox.Show("Root name have to ends with ':/' symbol as per Snap2HTML template requirements", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!File.Exists(_text_InputFilePath.Text))
            {
                MessageBox.Show("Input file does not exist. Please provide correct input file path", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (File.Exists(_text_OutputFilePath.Text))
            {
                return (MessageBox.Show("Output file exist! Do you wish to OVERWRITE output file?", "Overwrite output file?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes);
            }

            return true;
        }

        private void _button_OutputFilePath_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                _text_OutputFilePath.Text = saveFileDialog1.FileName;
            }
        }

        private void _ts_Button_About_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Acerept2HTML (c) SOCAR 2022\r\nwww.socar.pl\r\nVer {Program.Version}\r\nTool to convert acerep PC-3000 files to HTML based on Snap2HTML template \r\nhttps://gitlab.com/socar_pl/acerep2html\r\nMIT license");
        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.AllowDrop = true;
            //this.DragEnter += new DragEventHandler(MainForm_DragEnter);
            //this.DragDrop += new DragEventHandler(Form1_DragDrop);
        }


        private void RefreshRootNameTextInput()
        {
            if (Program.GlobalAppSettings.UseStaticFolderName)
            {
                this._text_RootName.Text = Program.GlobalAppSettings.RootFolderName;
                this._text_RootName.Enabled = false;
            }
            else
            {
                this._text_RootName.Enabled = true;
                if (!String.IsNullOrEmpty(_text_InputFilePath.Text))
                {
                    try
                    {
                        FileInfo fi = new FileInfo(_text_InputFilePath.Text);
                        _text_RootName.Text = fi.Name.Substring(0, fi.Name.LastIndexOf('.'))+":/";
                    }
                    catch { }
                }

            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = "explorer.exe";
            psi.Arguments = "https://gitlab.com/socar_pl/acerep2html";
            Process.Start(psi);
        }

        private void MainForm_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) 
                e.Effect = DragDropEffects.Copy;
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            foreach (string file in files)
            {
                if(!String.IsNullOrEmpty(file) && file.ToLower().EndsWith("acerep"))
                {
                    _text_InputFilePath.Text = file;
                    RefreshUIAfterInputFileSet();
                    break;
                }
            }

        }
    }
}